import { Injectable } from '@angular/core';
import axios from 'axios';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class NurseService {

  private axiosInstance = axios.create({
    baseURL: `${environment.apiUrl}/nurse`
  })

  constructor () {
    this.axiosInstance.interceptors.request.use(config => {
      const token = sessionStorage.getItem('token')
      if (token) {
        config.headers['Authorization'] = `Bearer ${token}`
      }
      return config
    });

    this.axiosInstance.interceptors.response.use(response => {
      return response
    }, error => {
      return Promise.reject(error)
    })
  }

  async getActive(wardId: any, query: any, limit: any, offset: any) {
    const url = `/admit?ward_id=${wardId}&query=${query}&limit=${limit}&offset=${offset}`
    return await this.axiosInstance.get(url)
  }

  async getWaiting(limit: any, offset: any) {
    const url = `/waiting?limit=${limit}&offset=${offset}`
    return await this.axiosInstance.get(url)
  }

}
