import { Component } from '@angular/core';
import { UserProfileService } from '../../core/services/user-profiles.service';
import { UserProfileApiService } from '../../shared/services/user-profile.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-nurse',
  templateUrl: './nurse.component.html',
  styleUrls: ['./nurse.component.css']
})
export class NurseComponent {
  jwtHelper: JwtHelperService = new JwtHelperService();
  user_login_name: any;
  user_id: any

  constructor(

    private userProfileService: UserProfileService,
    private userProfileApiService: UserProfileApiService
  ) {
    const token: any = sessionStorage.getItem('token')
    const decoded = this.jwtHelper.decodeToken(token);
    this.user_login_name = this.userProfileService.fname;
    this.user_id = decoded.sub;
    // this.getUserById();
  }

  async getUserById() {
    try {
      let data = await this.userProfileApiService.getUserById(this.user_id);
      console.log(data);
      this.userProfileService.user_login_name = data.data[0].title + '' + data.data[0].fname + ' ' + data.data[0].lname;

      console.log(this.userProfileService.user_login_name);
      // sessionStorage.setItem('userLoginName', data.data[0].title + '' + data.data[0].fname + ' ' + data.data[0].lname);

    } catch (error) {
      console.log(error);

    }

  }

}
